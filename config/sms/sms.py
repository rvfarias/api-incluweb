import boto3
import secrets

def gera_token():
    return secrets.randbelow(10**6)

def send_sms(phone_number):
    sns = boto3.client('sns', region_name='us-east-1')
    token = gera_token()
    message = f"Seu token de acesso ao incluwed é: {token:06d}"

    try:
        response = sns.publish(
            PhoneNumber=phone_number,
            Message=message
        )

        return True
    except Exception as e:
        return False